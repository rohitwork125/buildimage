#!/bin/bash

help() {
        echo "./run.sh [OPTIONS]"
        echo "    -t <id> tunneling device id(0~255)"
        echo "    -p <num> g2dm socket port number"
        echo "    -P <num> ril socket port number"
        echo "    -f <file path> log file path"
        echo "    -l <level> log level"
        echo "       2:CRIT 3:ERR 4:WARN 5:NOTI 6:INFO 7:DBG"
        echo "    -s <size> log split size(default:50(M))"
        echo "    -m <mode> log printf timestamp mode(default:disabled(0))"
        echo "    -b set tun/tap bridge mode" 
        exit 0
}

TUN_DEV_ID=0
#port=7070
port=0
ril_port=0
AP_LOG_LEVEL=7
AP_LOG_SIZE=50
AP_LOG_MODE=0
bridge=0
while getopts "t:p:P:xf:l:s:m:b" opt
do
case $opt in
t)
	TUN_DEV_ID=$OPTARG
	;;
p)
	port=$OPTARG
	;;
P)
	ril_port=$OPTARG
	;;
f)
	export AP_LOG_FILE=$OPTARG
	export AP_LOG_TIME=`echo $(date -u +%Y)_$(date -u +%m)_$(date -u +%d)_$(date -u +%H)_$(date -u +%M)_$(date -u +%S)`
	export AP_LOG_MODE=1
	;;
l)
	export AP_LOG_LEVEL=$OPTARG
	;;
s)
	export AP_LOG_SIZE=$OPTARG
	;;
m)
	export AP_LOG_MODE=$OPTARG
	;;
b)
	export bridge=1
	;;
h) help ;;
?) help ;;
esac
done

./kill.sh -t $TUN_DEV_ID 2> /dev/null

export HOSTDIR=`pwd`
#HOSTDIR=`pwd`
OUTPUTDIR=$HOSTDIR/output

export PATH=$PATH:$OUTPUTDIR
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OUTPUTDIR

cd $OUTPUTDIR

echo "TUN_DEV_ID=$TUN_DEV_ID"
echo "AP_LOG_FILE=$AP_LOG_FILE"
rm -f $AP_LOG_FILE
echo "AP_LOG_TIME=$AP_LOG_TIME"
echo "AP_LOG_LEVEL=$AP_LOG_LEVEL"
echo "AP_LOG_SIZE=$AP_LOG_SIZE"
echo "AP_LOG_MODE=$AP_LOG_MODE"

sysctl -w net.core.rmem_default=4194304
sysctl -w net.core.rmem_max=4194304
sysctl -w net.core.wmem_default=4194304
sysctl -w net.core.wmem_max=4194304

if [ -f gdmlte.ko ]; then
insmod gdmlte.ko
fi

export LTE_PARAM_DIR=../param
$HOSTDIR/board.conf $TUN_DEV_ID $port $ril_port $bridge

