nvm bcfgw 48 1

//
// NV version
//
nvm calw 0 18

//
// Board configuration NV parameter
//
//
nvm bcfgw 0 0 0 0
nvm bcfgw 1 0 5 5
nvm bcfgw 2 0 0 0 0
nvm bcfgw 3 0 8388608
nvm bcfgw 4 0 0 0 0 0 0
nvm bcfgw 5 0 0
nvm bcfgw 6 0 23 10 10 -54
nvm bcfgw 7 0 2 2
nvm bcfgw 8 0 1 0 0
nvm bcfgw 33 0 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 0 0 0 0 0 0 0 0 0
nvm bcfgw 39 0 0 0 0 0 0 0
nvm bcfgw 40 0 1026
nvm bcfgw 41 0 0 2
nvm bcfgw 42 0 0 0
nvm bcfgw 45 0 85 3 0
nvm bcfgw 46 0 0
//
nvm bcfgw 0 1 1 0
nvm bcfgw 1 1 5 5
nvm bcfgw 2 1 0 0 0
nvm bcfgw 3 1 8388608
nvm bcfgw 4 1 0 0 0 0 0
nvm bcfgw 5 1 0
nvm bcfgw 6 1 23 15 5 -54
nvm bcfgw 7 1 1 0
nvm bcfgw 8 1 2 0 0
nvm bcfgw 33 1 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 1 0 0 0 0 0 0 0 0
nvm bcfgw 39 1 0 0 0 0 0 0
nvm bcfgw 40 1 0
nvm bcfgw 41 1 0 0
nvm bcfgw 42 1 0 0
nvm bcfgw 45 1 85 3 0
nvm bcfgw 46 1 0
//
nvm bcfgw 0 2 10 10
nvm bcfgw 1 2 20 20
nvm bcfgw 2 2 0 0 0
nvm bcfgw 3 2 0
nvm bcfgw 4 2 0 0 0 0 0
nvm bcfgw 5 2 0
nvm bcfgw 6 2 23 15 5 -54
nvm bcfgw 7 2 2 2
nvm bcfgw 8 2 16 0 0
nvm bcfgw 33 2 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 2 0 0 0 0 0 0 0 0
nvm bcfgw 39 2 0 0 0 0 0 0
nvm bcfgw 40 2 8
nvm bcfgw 41 2 0 2
nvm bcfgw 42 2 0 0
nvm bcfgw 45 2 85 3 0
nvm bcfgw 46 2 0
//
nvm bcfgw 0 3 11 0
nvm bcfgw 1 3 20 20
nvm bcfgw 2 3 0 0 0
nvm bcfgw 3 3 0
nvm bcfgw 4 3 0 0 0 0 0
nvm bcfgw 5 3 0
nvm bcfgw 6 3 23 15 5 -54
nvm bcfgw 7 3 1 0
nvm bcfgw 8 3 32 0 0
nvm bcfgw 33 3 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 3 0 0 0 0 0 0 0 0
nvm bcfgw 39 3 0 0 0 0 0 0
nvm bcfgw 40 3 0
nvm bcfgw 41 3 0 0
nvm bcfgw 42 3 0 0
nvm bcfgw 45 3 0 0 0
nvm bcfgw 46 3 0
//
nvm bcfgw 0 4 2 3
nvm bcfgw 1 4 1 1
nvm bcfgw 2 4 0 0 1
nvm bcfgw 3 4 0
nvm bcfgw 4 4 0 0 0 0 0
nvm bcfgw 5 4 0
nvm bcfgw 6 4 23 15 5 -54
nvm bcfgw 7 4 2 4
nvm bcfgw 8 4 4 0 0
nvm bcfgw 33 4 1 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 4 255 1 0 0 0 0 0 0
nvm bcfgw 35 0 2 26 34 52 11 0
nvm bcfgw 36 0 0 1 0 0
nvm bcfgw 37 0 16 16 0 0
nvm bcfgw 39 4 0 0 0 0 0 0
nvm bcfgw 40 4 1130
nvm bcfgw 41 4 0 6
nvm bcfgw 42 4 0 0
nvm bcfgw 45 4 85 3 0
nvm bcfgw 46 4 0
//
nvm bcfgw 0 5 13 14
nvm bcfgw 1 5 1 1
nvm bcfgw 2 5 0 0 1
nvm bcfgw 3 5 0
nvm bcfgw 4 5 0 0 0 0 0
nvm bcfgw 5 5 0
nvm bcfgw 6 5 23 15 5 -54
nvm bcfgw 7 5 1 2
nvm bcfgw 8 5 128 0 0
nvm bcfgw 33 5 1 0 0 0 0 0 0 1 0 0 0 0 0
nvm bcfgw 34 5 1 1 1 0 0 0 0 0
nvm bcfgw 35 1 2 26 34 52 11 0
nvm bcfgw 36 1 0 1 0 0
nvm bcfgw 37 1 16 16 0 0
nvm bcfgw 39 5 0 0 0 0 0 0
nvm bcfgw 40 5 0
nvm bcfgw 41 5 0 2
nvm bcfgw 42 5 0 0
nvm bcfgw 45 5 85 3 0
nvm bcfgw 46 5 0
//
nvm bcfgw 0 6 3 4
nvm bcfgw 1 6 3 3
nvm bcfgw 2 6 0 0 1
nvm bcfgw 3 6 0
nvm bcfgw 4 6 0 0 0 0 0
nvm bcfgw 5 6 0
nvm bcfgw 6 6 23 15 5 -54
nvm bcfgw 7 6 2 4
nvm bcfgw 8 6 8 0 0
nvm bcfgw 33 6 1 0 0 0 0 0 0 2 0 0 0 0 0
nvm bcfgw 34 6 0 1 2 0 0 0 0 0
nvm bcfgw 35 2 2 26 34 52 11 0
nvm bcfgw 36 2 0 1 0 0
nvm bcfgw 37 2 11 11 0 0
nvm bcfgw 39 6 0 0 0 0 0 0
nvm bcfgw 40 6 17556
nvm bcfgw 41 6 0 6
nvm bcfgw 42 6 0 0
nvm bcfgw 45 6 85 3 0
nvm bcfgw 46 6 0
//
nvm bcfgw 0 7 12 13
nvm bcfgw 1 7 3 3
nvm bcfgw 2 7 0 0 1
nvm bcfgw 3 7 0
nvm bcfgw 4 7 0 0 0 0 0
nvm bcfgw 5 7 0
nvm bcfgw 6 7 23 15 5 -54
nvm bcfgw 7 7 1 2
nvm bcfgw 8 7 64 0 0
nvm bcfgw 33 7 1 0 0 0 0 0 0 3 0 0 0 0 0
nvm bcfgw 34 7 1 1 3 0 0 0 0 0
nvm bcfgw 35 3 2 26 34 52 11 0
nvm bcfgw 36 3 0 1 0 0
nvm bcfgw 37 3 11 11 0 0
nvm bcfgw 39 7 0 0 0 0 0 0
nvm bcfgw 40 7 0
nvm bcfgw 41 7 0 2
nvm bcfgw 42 7 0 0
nvm bcfgw 45 7 85 3 0
nvm bcfgw 46 7 0
//
nvm bcfgw 0 8 4 5
nvm bcfgw 1 8 7 7
nvm bcfgw 2 8 0 0 1
nvm bcfgw 3 8 4194304
nvm bcfgw 4 8 0 0 0 0 0
nvm bcfgw 5 8 0
nvm bcfgw 6 8 23 7 -3 -54
nvm bcfgw 7 8 4 4
nvm bcfgw 8 8 0 0 0
nvm bcfgw 33 8 2 0 0 0 0 0 0 4 5 0 0 0 0
nvm bcfgw 34 8 0 2 4 5 0 0 0 0
nvm bcfgw 35 4 2 26 34 52 11 0
nvm bcfgw 36 4 0 1 0 0
nvm bcfgw 37 4 2 2 0 0
nvm bcfgw 35 5 2 26 33 52 10 0
nvm bcfgw 36 5 0 1 0 0
nvm bcfgw 37 5 4 4 0 0
nvm bcfgw 39 8 0 10000 13 31 144 165
nvm bcfgw 38 0 0 0 2 0 1 3 0 0
nvm bcfgw 38 0 1 4 8 68 122 149 0 0
nvm bcfgw 38 0 2 4 8 68 53 144 0 0
nvm bcfgw 38 0 3 4 8 68 20 144 0 0
nvm bcfgw 38 0 4 4 7 0 0 0 0 0
nvm bcfgw 40 8 1032
nvm bcfgw 41 8 0 6
nvm bcfgw 42 8 0 0
nvm bcfgw 45 8 85 3 0
nvm bcfgw 46 8 0
//
nvm bcfgw 0 9 5 0
nvm bcfgw 1 9 7 7
nvm bcfgw 2 9 0 0 1
nvm bcfgw 3 9 4194304
nvm bcfgw 4 9 0 0 0 0 0
nvm bcfgw 5 9 0
nvm bcfgw 6 9 23 7 -3 -54
nvm bcfgw 7 9 1 0
nvm bcfgw 8 9 0 0 0
nvm bcfgw 33 9 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 9 0 0 0 0 0 0 0 0
nvm bcfgw 39 9 0 10001 13 31 144 165
nvm bcfgw 38 1 0 0 2 0 1 3 0 0
nvm bcfgw 38 1 1 4 8 68 122 149 0 0
nvm bcfgw 38 1 2 4 8 68 53 144 0 0
nvm bcfgw 38 1 3 4 8 68 20 144 0 0
nvm bcfgw 38 1 4 4 7 0 0 0 0 0
nvm bcfgw 40 9 0
nvm bcfgw 41 9 0 0
nvm bcfgw 42 9 0 0
nvm bcfgw 45 9 85 3 0
nvm bcfgw 46 9 0
//
nvm bcfgw 0 10 14 15
nvm bcfgw 1 10 7 7
nvm bcfgw 2 10 0 0 1
nvm bcfgw 3 10 4
nvm bcfgw 4 10 0 0 0 0 0
nvm bcfgw 5 10 0
nvm bcfgw 6 10 23 7 -3 -54
nvm bcfgw 7 10 1 2
nvm bcfgw 8 10 0 0 0
nvm bcfgw 33 10 2 0 0 0 0 0 0 6 7 0 0 0 0
nvm bcfgw 34 10 1 2 6 7 0 0 0 0
nvm bcfgw 35 6 2 26 34 52 11 0
nvm bcfgw 36 6 0 1 0 0
nvm bcfgw 37 6 2 2 0 0
nvm bcfgw 35 7 2 26 33 52 10 0
nvm bcfgw 36 7 0 1 0 0
nvm bcfgw 37 7 4 4 0 0
nvm bcfgw 39 10 1 10000 13 31 144 165
nvm bcfgw 38 0 0 0 2 0 1 3 0 0
nvm bcfgw 38 0 1 4 8 68 122 149 0 0
nvm bcfgw 38 0 2 4 8 68 53 144 0 0
nvm bcfgw 38 0 3 4 8 68 20 144 0 0
nvm bcfgw 38 0 4 4 7 0 0 0 0 0
nvm bcfgw 40 10 0
nvm bcfgw 41 10 0 2
nvm bcfgw 42 10 0 0
nvm bcfgw 45 10 85 3 0
nvm bcfgw 46 10 0
//
nvm bcfgw 0 11 15 0
nvm bcfgw 1 11 7 7
nvm bcfgw 2 11 0 0 1
nvm bcfgw 3 11 4
nvm bcfgw 4 11 0 0 0 0 0
nvm bcfgw 5 11 0
nvm bcfgw 6 11 23 7 -3 -54
nvm bcfgw 7 11 1 0
nvm bcfgw 8 11 512 0 0
nvm bcfgw 33 11 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 11 0 0 0 0 0 0 0 0
nvm bcfgw 39 11 1 10001 13 31 144 165
nvm bcfgw 38 1 0 0 2 0 1 3 0 0
nvm bcfgw 38 1 1 4 8 68 122 149 0 0
nvm bcfgw 38 1 2 4 8 68 53 144 0 0
nvm bcfgw 38 1 3 4 8 68 20 144 0 0
nvm bcfgw 38 1 4 4 7 0 0 0 0 0
nvm bcfgw 40 11 0
nvm bcfgw 41 11 0 0
nvm bcfgw 42 11 0 0
nvm bcfgw 45 11 85 3 0
nvm bcfgw 46 11 0
//
nvm bcfgw 0 12 4 5
nvm bcfgw 1 12 40 40
nvm bcfgw 2 12 0 0 1
nvm bcfgw 3 12 0
nvm bcfgw 4 12 0 0 0 0 0
nvm bcfgw 5 12 0
nvm bcfgw 6 12 23 7 -3 -54
nvm bcfgw 7 12 4 4
nvm bcfgw 8 12 0 0 0
nvm bcfgw 33 12 3 0 0 0 0 0 0 8 9 10 0 0 0
nvm bcfgw 34 12 0 3 8 9 10 0 0 0
nvm bcfgw 35 8 2 26 34 52 11 0
nvm bcfgw 36 8 0 1 0 0
nvm bcfgw 37 8 4 4 0 0
nvm bcfgw 35 9 2 26 33 52 10 0
nvm bcfgw 36 9 0 1 0 0
nvm bcfgw 37 9 1 1 0 0
nvm bcfgw 35 10 1 29 144 165 13 0
nvm bcfgw 36 10 2 0 0 0
nvm bcfgw 37 10 12 0 0 0
nvm bcfgw 39 12 0 10002 13 31 144 165
nvm bcfgw 38 2 0 0 2 0 1 3 0 0
nvm bcfgw 38 2 1 4 9 20 183 151 0 0
nvm bcfgw 38 2 2 4 9 20 52 144 0 0
nvm bcfgw 38 2 3 4 9 20 19 144 0 0
nvm bcfgw 38 2 4 4 12 0 0 0 0 0
nvm bcfgw 40 12 16384
nvm bcfgw 41 12 0 6
nvm bcfgw 42 12 0 0
nvm bcfgw 45 12 85 3 0
nvm bcfgw 46 12 0
//
nvm bcfgw 0 13 5 0
nvm bcfgw 1 13 40 40
nvm bcfgw 2 13 0 0 1
nvm bcfgw 3 13 0
nvm bcfgw 4 13 0 0 0 0 0
nvm bcfgw 5 13 0
nvm bcfgw 6 13 23 7 -3 -54
nvm bcfgw 7 13 1 0
nvm bcfgw 8 13 0 0 0
nvm bcfgw 33 13 1 0 0 0 0 0 0 11 0 0 0 0 0
nvm bcfgw 34 13 0 1 11 0 0 0 0 0
nvm bcfgw 35 11 1 29 144 165 13 0
nvm bcfgw 36 11 2 0 0 0
nvm bcfgw 37 11 12 0 0 0
nvm bcfgw 39 13 0 10003 13 31 144 165
nvm bcfgw 38 3 0 0 2 0 1 3 0 0
nvm bcfgw 38 3 1 4 9 20 183 151 0 0
nvm bcfgw 38 3 2 4 9 20 52 144 0 0
nvm bcfgw 38 3 3 4 9 20 19 144 0 0
nvm bcfgw 38 3 4 4 12 0 0 0 0 0
nvm bcfgw 40 13 0
nvm bcfgw 41 13 0 0
nvm bcfgw 42 13 0 0
nvm bcfgw 45 13 85 3 0
nvm bcfgw 46 13 0
//
nvm bcfgw 0 14 14 15
nvm bcfgw 1 14 40 40
nvm bcfgw 2 14 0 0 1
nvm bcfgw 3 14 0
nvm bcfgw 4 14 0 0 0 0 0
nvm bcfgw 5 14 0
nvm bcfgw 6 14 23 7 -3 -54
nvm bcfgw 7 14 1 2
nvm bcfgw 8 14 0 0 0
nvm bcfgw 33 14 3 0 0 0 0 0 0 12 13 14 0 0 0
nvm bcfgw 34 14 1 3 12 13 14 0 0 0
nvm bcfgw 35 12 2 26 34 52 11 0
nvm bcfgw 36 12 0 1 0 0
nvm bcfgw 37 12 4 4 0 0
nvm bcfgw 35 13 2 26 33 52 10 0
nvm bcfgw 36 13 0 1 0 0
nvm bcfgw 37 13 1 1 0 0
nvm bcfgw 35 14 1 29 144 165 13 0
nvm bcfgw 36 14 2 0 0 0
nvm bcfgw 37 14 12 0 0 0
nvm bcfgw 39 14 1 10002 13 31 144 165
nvm bcfgw 38 2 0 0 2 0 1 3 0 0
nvm bcfgw 38 2 1 4 9 20 183 151 0 0
nvm bcfgw 38 2 2 4 9 20 52 144 0 0
nvm bcfgw 38 2 3 4 9 20 19 144 0 0
nvm bcfgw 38 2 4 4 12 0 0 0 0 0
nvm bcfgw 40 14 0
nvm bcfgw 41 14 0 2
nvm bcfgw 42 14 0 0
nvm bcfgw 45 14 85 3 0
nvm bcfgw 46 14 0
//
nvm bcfgw 0 15 15 0
nvm bcfgw 1 15 40 40
nvm bcfgw 2 15 0 0 1
nvm bcfgw 3 15 0
nvm bcfgw 4 15 0 0 0 0 0
nvm bcfgw 5 15 0
nvm bcfgw 6 15 23 7 -3 -54
nvm bcfgw 7 15 1 0
nvm bcfgw 8 15 0 0 0
nvm bcfgw 33 15 1 0 0 0 0 0 0 15 0 0 0 0 0
nvm bcfgw 34 15 1 1 15 0 0 0 0 0
nvm bcfgw 35 15 1 29 144 165 13 0
nvm bcfgw 36 15 2 0 0 0
nvm bcfgw 37 15 12 0 0 0
nvm bcfgw 39 15 1 10003 13 31 144 165
nvm bcfgw 38 3 0 0 2 0 1 3 0 0
nvm bcfgw 38 3 1 4 9 20 183 151 0 0
nvm bcfgw 38 3 2 4 9 20 52 144 0 0
nvm bcfgw 38 3 3 4 9 20 19 144 0 0
nvm bcfgw 38 3 4 4 12 0 0 0 0 0
nvm bcfgw 40 15 0
nvm bcfgw 41 15 0 0
nvm bcfgw 42 15 0 0
nvm bcfgw 45 15 85 3 0
nvm bcfgw 46 15 0
//
nvm bcfgw 0 16 4 5
nvm bcfgw 1 16 38 38
nvm bcfgw 2 16 0 0 1
nvm bcfgw 3 16 0
nvm bcfgw 4 16 0 0 0 0 0
nvm bcfgw 5 16 0
nvm bcfgw 6 16 23 7 -3 -54
nvm bcfgw 7 16 4 4
nvm bcfgw 8 16 0 0 0
nvm bcfgw 33 16 3 0 0 0 0 0 0 24 25 26 0 0 0
nvm bcfgw 34 16 0 3 24 25 26 0 0 0
nvm bcfgw 35 24 2 26 34 52 11 0
nvm bcfgw 36 24 0 1 0 0
nvm bcfgw 37 24 1 1 0 0
nvm bcfgw 35 25 2 26 33 52 10 0
nvm bcfgw 36 25 0 1 0 0
nvm bcfgw 37 25 2 2 0 0
nvm bcfgw 35 26 2 29 144 165 13 0
nvm bcfgw 36 26 2 0 0 0
nvm bcfgw 37 26 13 0 0 0
nvm bcfgw 39 16 0 10006 13 31 144 165
nvm bcfgw 38 6 0 0 2 0 1 3 0 0
nvm bcfgw 38 6 1 4 10 28 123 150 0 0
nvm bcfgw 38 6 2 4 10 28 53 144 0 0
nvm bcfgw 38 6 3 4 10 28 20 144 0 0
nvm bcfgw 38 6 4 4 13 0 0 0 0 0
nvm bcfgw 40 16 262144
nvm bcfgw 41 16 0 6
nvm bcfgw 42 16 0 0
nvm bcfgw 45 16 85 3 0
nvm bcfgw 46 16 0
//
nvm bcfgw 0 17 5 0
nvm bcfgw 1 17 38 38
nvm bcfgw 2 17 0 0 1
nvm bcfgw 3 17 0
nvm bcfgw 4 17 0 0 0 0 0
nvm bcfgw 5 17 0
nvm bcfgw 6 17 23 7 -3 -54
nvm bcfgw 7 17 1 0
nvm bcfgw 8 17 0 0 1
nvm bcfgw 33 17 1 0 0 0 0 0 0 27 0 0 0 0 0
nvm bcfgw 34 17 0 1 27 0 0 0 0 0
nvm bcfgw 35 27 2 29 144 165 13 0
nvm bcfgw 36 27 2 0 0 0
nvm bcfgw 37 27 13 0 0 0
nvm bcfgw 39 17 0 10007 13 31 144 165
nvm bcfgw 38 7 0 0 2 0 1 3 0 0
nvm bcfgw 38 7 1 4 10 28 123 150 0 0
nvm bcfgw 38 7 2 4 10 28 53 144 0 0
nvm bcfgw 38 7 3 4 10 28 20 144 0 0
nvm bcfgw 38 7 4 4 13 0 0 0 0 0
nvm bcfgw 40 17 0
nvm bcfgw 41 17 0 0
nvm bcfgw 42 17 0 0
nvm bcfgw 45 17 85 3 0
nvm bcfgw 46 17 0
//
nvm bcfgw 0 18 14 15
nvm bcfgw 1 18 38 38
nvm bcfgw 2 18 0 0 1
nvm bcfgw 3 18 0
nvm bcfgw 4 18 0 0 0 0 0
nvm bcfgw 5 18 0
nvm bcfgw 6 18 23 7 -3 -54
nvm bcfgw 7 18 1 2
nvm bcfgw 8 18 0 0 0
nvm bcfgw 33 18 3 0 0 0 0 0 0 28 29 30 0 0 0
nvm bcfgw 34 18 1 3 28 29 30 0 0 0
nvm bcfgw 35 28 2 26 34 52 11 0
nvm bcfgw 36 28 0 1 0 0
nvm bcfgw 37 28 1 1 0 0
nvm bcfgw 35 29 2 26 33 52 10 0
nvm bcfgw 36 29 0 1 0 0
nvm bcfgw 37 29 2 2 0 0
nvm bcfgw 35 30 2 29 144 165 13 0
nvm bcfgw 36 30 2 0 0 0
nvm bcfgw 37 30 13 0 0 0
nvm bcfgw 39 18 1 10006 13 31 144 165
nvm bcfgw 38 6 0 0 2 0 1 3 0 0
nvm bcfgw 38 6 1 4 10 28 123 150 0 0
nvm bcfgw 38 6 2 4 10 28 53 144 0 0
nvm bcfgw 38 6 3 4 10 28 20 144 0 0
nvm bcfgw 38 6 4 4 13 0 0 0 0 0
nvm bcfgw 40 18 0
nvm bcfgw 41 18 0 2
nvm bcfgw 42 18 0 0
nvm bcfgw 45 18 85 3 0
nvm bcfgw 46 18 0
//
nvm bcfgw 0 19 15 0
nvm bcfgw 1 19 38 38
nvm bcfgw 2 19 0 0 1
nvm bcfgw 3 19 0
nvm bcfgw 4 19 0 0 0 0 0
nvm bcfgw 5 19 0
nvm bcfgw 6 19 23 7 -3 -54
nvm bcfgw 7 19 1 0
nvm bcfgw 8 19 0 0 0
nvm bcfgw 33 19 1 0 0 0 0 0 0 31 0 0 0 0 0
nvm bcfgw 34 19 1 1 31 0 0 0 0 0
nvm bcfgw 35 31 2 29 144 165 13 0
nvm bcfgw 36 31 2 0 0 0
nvm bcfgw 37 31 13 0 0 0
nvm bcfgw 39 19 1 10007 13 31 144 165
nvm bcfgw 38 7 0 0 2 0 1 3 0 0
nvm bcfgw 38 7 1 4 10 28 123 150 0 0
nvm bcfgw 38 7 2 4 10 28 53 144 0 0
nvm bcfgw 38 7 3 4 10 28 20 144 0 0
nvm bcfgw 38 7 4 4 13 0 0 0 0 0
nvm bcfgw 40 19 0
nvm bcfgw 41 19 0 0
nvm bcfgw 42 19 0 0
nvm bcfgw 45 19 85 3 0
nvm bcfgw 46 19 0
//
nvm bcfgw 0 20 4 5
nvm bcfgw 1 20 41 41
nvm bcfgw 2 20 0 0 1
nvm bcfgw 3 20 0
nvm bcfgw 4 20 0 0 0 0 0
nvm bcfgw 5 20 0
nvm bcfgw 6 20 23 7 -3 -54
nvm bcfgw 7 20 4 4
nvm bcfgw 8 20 0 0 0
nvm bcfgw 33 20 3 0 0 0 0 0 0 16 17 18 0 0 0
nvm bcfgw 34 20 0 3 16 17 18 0 0 0
nvm bcfgw 35 16 2 26 34 52 11 0
nvm bcfgw 36 16 0 1 0 0
nvm bcfgw 37 16 7 7 0 0
nvm bcfgw 35 17 2 26 33 52 10 0
nvm bcfgw 36 17 0 1 0 0
nvm bcfgw 37 17 2 2 0 0
nvm bcfgw 35 18 2 29 144 165 13 0
nvm bcfgw 36 18 2 0 0 0
nvm bcfgw 37 18 14 0 0 0
nvm bcfgw 39 20 0 10004 13 31 144 165
nvm bcfgw 38 4 0 0 2 0 1 3 0 0
nvm bcfgw 38 4 1 4 11 12 122 148 0 0
nvm bcfgw 38 4 2 4 11 12 53 144 0 0
nvm bcfgw 38 4 3 4 11 12 36 144 0 0
nvm bcfgw 38 4 4 4 14 0 0 0 0 0
nvm bcfgw 40 20 4194312
nvm bcfgw 41 20 0 6
nvm bcfgw 42 20 0 0
nvm bcfgw 45 20 85 3 0
nvm bcfgw 46 20 0
//
nvm bcfgw 0 21 5 0
nvm bcfgw 1 21 41 41
nvm bcfgw 2 21 0 0 1
nvm bcfgw 3 21 0
nvm bcfgw 4 21 0 0 0 0 0
nvm bcfgw 5 21 0
nvm bcfgw 6 21 23 7 -3 -54
nvm bcfgw 7 21 1 0
nvm bcfgw 8 21 0 0 0
nvm bcfgw 33 21 1 0 0 0 0 0 0 19 0 0 0 0 0
nvm bcfgw 34 21 0 1 19 0 0 0 0 0
nvm bcfgw 35 19 2 29 144 165 13 0
nvm bcfgw 36 19 2 0 0 0
nvm bcfgw 37 19 14 0 0 0
nvm bcfgw 39 21 0 10005 13 31 144 165
nvm bcfgw 38 5 0 0 2 0 1 3 0 0
nvm bcfgw 38 5 1 4 11 12 122 148 0 0
nvm bcfgw 38 5 2 4 11 12 53 144 0 0
nvm bcfgw 38 5 3 4 11 12 36 144 0 0
nvm bcfgw 38 5 4 4 14 0 0 0 0 0
nvm bcfgw 40 21 0
nvm bcfgw 41 21 0 0
nvm bcfgw 42 21 0 0
nvm bcfgw 45 21 85 3 0
nvm bcfgw 46 21 0
//
nvm bcfgw 0 22 14 15
nvm bcfgw 1 22 41 41
nvm bcfgw 2 22 0 0 1
nvm bcfgw 3 22 0
nvm bcfgw 4 22 0 0 0 0 0
nvm bcfgw 5 22 0
nvm bcfgw 6 22 23 7 -3 -54
nvm bcfgw 7 22 1 2
nvm bcfgw 8 22 0 0 0
nvm bcfgw 33 22 3 0 0 0 0 0 0 20 21 22 0 0 0
nvm bcfgw 34 22 1 3 20 21 22 0 0 0
nvm bcfgw 35 20 2 26 34 52 11 0
nvm bcfgw 36 20 0 1 0 0
nvm bcfgw 37 20 7 7 0 0
nvm bcfgw 35 21 2 26 33 52 10 0
nvm bcfgw 36 21 0 1 0 0
nvm bcfgw 37 21 2 2 0 0
nvm bcfgw 35 22 2 29 144 165 13 0
nvm bcfgw 36 22 2 0 0 0
nvm bcfgw 37 22 14 0 0 0
nvm bcfgw 39 22 1 10004 13 31 144 165
nvm bcfgw 38 4 0 0 2 0 1 3 0 0
nvm bcfgw 38 4 1 4 11 12 122 148 0 0
nvm bcfgw 38 4 2 4 11 12 53 144 0 0
nvm bcfgw 38 4 3 4 11 12 36 144 0 0
nvm bcfgw 38 4 4 4 14 0 0 0 0 0
nvm bcfgw 40 22 0
nvm bcfgw 41 22 0 2
nvm bcfgw 42 22 0 0
nvm bcfgw 45 22 85 3 0
nvm bcfgw 46 22 0
//
nvm bcfgw 0 23 15 0
nvm bcfgw 1 23 41 41
nvm bcfgw 2 23 0 0 1
nvm bcfgw 3 23 0
nvm bcfgw 4 23 0 0 0 0 0
nvm bcfgw 5 23 0
nvm bcfgw 6 23 23 7 -3 -54
nvm bcfgw 7 23 1 0
nvm bcfgw 8 23 0 0 0
nvm bcfgw 33 23 1 0 0 0 0 0 0 23 0 0 0 0 0
nvm bcfgw 34 23 1 1 23 0 0 0 0 0
nvm bcfgw 35 23 2 29 144 165 13 0
nvm bcfgw 36 23 2 0 0 0
nvm bcfgw 37 23 14 0 0 0
nvm bcfgw 39 23 1 10005 13 31 144 165
nvm bcfgw 38 5 0 0 2 0 1 3 0 0
nvm bcfgw 38 5 1 4 11 12 122 148 0 0
nvm bcfgw 38 5 2 4 11 12 53 144 0 0
nvm bcfgw 38 5 3 4 11 12 36 144 0 0
nvm bcfgw 38 5 4 4 14 0 0 0 0 0
nvm bcfgw 40 23 0
nvm bcfgw 41 23 0 0
nvm bcfgw 42 23 0 0
nvm bcfgw 45 23 85 3 0
nvm bcfgw 46 23 0
//
nvm bcfgw 0 24 6 8
nvm bcfgw 1 24 42 42
nvm bcfgw 2 24 1 1 1
nvm bcfgw 3 24 0
nvm bcfgw 4 24 0 0 0 0 0
nvm bcfgw 5 24 0
nvm bcfgw 6 24 23 7 -3 -54
nvm bcfgw 7 24 4 8
nvm bcfgw 8 24 0 4096 8192
nvm bcfgw 33 24 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 24 0 0 0 0 0 0 0 0
nvm bcfgw 39 24 0 10008 15 47 16 134
nvm bcfgw 38 8 0 0 0 1 0 0 0 0
nvm bcfgw 38 8 1 2 5 93 0 0 0 0
nvm bcfgw 38 8 2 2 7 127 0 0 0 0
nvm bcfgw 38 8 3 2 7 127 0 0 0 0
nvm bcfgw 38 8 4 4 0 0 0 0 0 0
nvm bcfgw 40 24 0
nvm bcfgw 41 24 0 0
nvm bcfgw 42 24 0 0
nvm bcfgw 45 24 85 3 0
nvm bcfgw 46 24 0
//
nvm bcfgw 0 25 7 9
nvm bcfgw 1 25 42 42
nvm bcfgw 2 25 1 1 1
nvm bcfgw 3 25 0
nvm bcfgw 4 25 0 0 0 0 0
nvm bcfgw 5 25 0
nvm bcfgw 6 25 23 7 -3 -54
nvm bcfgw 7 25 1 2
nvm bcfgw 8 25 0 16384 32768
nvm bcfgw 33 25 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 25 0 0 0 0 0 0 0 0
nvm bcfgw 39 25 0 10009 15 47 16 134
nvm bcfgw 38 9 0 0 0 1 0 0 0 0
nvm bcfgw 38 9 1 2 5 93 0 0 0 0
nvm bcfgw 38 9 2 2 5 127 0 0 0 0
nvm bcfgw 38 9 3 2 7 127 0 0 0 0
nvm bcfgw 38 9 4 4 0 0 0 0 0 0
nvm bcfgw 40 25 0
nvm bcfgw 41 25 0 0
nvm bcfgw 42 25 0 0
nvm bcfgw 45 25 85 3 0
nvm bcfgw 46 25 0
//
nvm bcfgw 0 26 16 18
nvm bcfgw 1 26 42 42
nvm bcfgw 2 26 1 1 1
nvm bcfgw 3 26 0
nvm bcfgw 4 26 0 0 0 0 0
nvm bcfgw 5 26 0
nvm bcfgw 6 26 23 7 -3 -54
nvm bcfgw 7 26 1 2
nvm bcfgw 8 26 0 65536 131072
nvm bcfgw 33 26 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 26 0 0 0 0 0 0 0 0
nvm bcfgw 39 26 1 10008 15 47 16 134
nvm bcfgw 38 8 0 0 0 1 0 0 0 0
nvm bcfgw 38 8 1 2 5 93 0 0 0 0
nvm bcfgw 38 8 2 2 7 127 0 0 0 0
nvm bcfgw 38 8 3 2 7 127 0 0 0 0
nvm bcfgw 38 8 4 4 0 0 0 0 0 0
nvm bcfgw 40 26 0
nvm bcfgw 41 26 0 0
nvm bcfgw 42 26 0 0
nvm bcfgw 45 26 85 3 0
nvm bcfgw 46 26 0
//
nvm bcfgw 0 27 17 19
nvm bcfgw 1 27 42 42
nvm bcfgw 2 27 1 1 1
nvm bcfgw 3 27 0
nvm bcfgw 4 27 0 0 0 0 0
nvm bcfgw 5 27 0
nvm bcfgw 6 27 23 7 -3 -54
nvm bcfgw 7 27 1 2
nvm bcfgw 8 27 0 262144 524288
nvm bcfgw 33 27 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 27 0 0 0 0 0 0 0 0
nvm bcfgw 39 27 1 10009 15 47 16 134
nvm bcfgw 38 9 0 0 0 1 0 0 0 0
nvm bcfgw 38 9 1 2 5 93 0 0 0 0
nvm bcfgw 38 9 2 2 5 127 0 0 0 0
nvm bcfgw 38 9 3 2 7 127 0 0 0 0
nvm bcfgw 38 9 4 4 0 0 0 0 0 0
nvm bcfgw 40 27 0
nvm bcfgw 41 27 0 0
nvm bcfgw 42 27 0 0
nvm bcfgw 45 27 85 3 0
nvm bcfgw 46 27 0
//
nvm bcfgw 0 28 6 8
nvm bcfgw 1 28 43 43
nvm bcfgw 2 28 1 1 1
nvm bcfgw 3 28 0
nvm bcfgw 4 28 0 0 0 0 0
nvm bcfgw 5 28 0
nvm bcfgw 6 28 23 7 -3 -54
nvm bcfgw 7 28 4 8
nvm bcfgw 8 28 0 4096 8192
nvm bcfgw 33 28 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 28 0 0 0 0 0 0 0 0
nvm bcfgw 39 28 0 10008 15 47 16 134
nvm bcfgw 38 8 0 0 0 1 0 0 0 0
nvm bcfgw 38 8 1 2 5 93 0 0 0 0
nvm bcfgw 38 8 2 2 7 127 0 0 0 0
nvm bcfgw 38 8 3 2 7 127 0 0 0 0
nvm bcfgw 38 8 4 4 0 0 0 0 0 0
nvm bcfgw 40 28 0
nvm bcfgw 41 28 0 0
nvm bcfgw 42 28 0 0
nvm bcfgw 45 28 85 3 0
nvm bcfgw 46 28 0
//
nvm bcfgw 0 29 7 9
nvm bcfgw 1 29 43 43
nvm bcfgw 2 29 1 1 1
nvm bcfgw 3 29 0
nvm bcfgw 4 29 0 0 0 0 0
nvm bcfgw 5 29 0
nvm bcfgw 6 29 23 7 -3 -54
nvm bcfgw 7 29 1 2
nvm bcfgw 8 29 0 16384 32768
nvm bcfgw 33 29 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 29 0 0 0 0 0 0 0 0
nvm bcfgw 39 29 0 10009 15 47 16 134
nvm bcfgw 38 9 0 0 0 1 0 0 0 0
nvm bcfgw 38 9 1 2 5 93 0 0 0 0
nvm bcfgw 38 9 2 2 5 127 0 0 0 0
nvm bcfgw 38 9 3 2 7 127 0 0 0 0
nvm bcfgw 38 9 4 4 0 0 0 0 0 0
nvm bcfgw 40 29 0
nvm bcfgw 41 29 0 0
nvm bcfgw 42 29 0 0
nvm bcfgw 45 29 85 3 0
nvm bcfgw 46 29 0
//
nvm bcfgw 0 30 16 18
nvm bcfgw 1 30 43 43
nvm bcfgw 2 30 1 1 1
nvm bcfgw 3 30 0
nvm bcfgw 4 30 0 0 0 0 0
nvm bcfgw 5 30 0
nvm bcfgw 6 30 23 7 -3 -54
nvm bcfgw 7 30 1 2
nvm bcfgw 8 30 0 65536 131072
nvm bcfgw 33 30 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 30 0 0 0 0 0 0 0 0
nvm bcfgw 39 30 1 10008 15 47 16 134
nvm bcfgw 38 8 0 0 0 1 0 0 0 0
nvm bcfgw 38 8 1 2 5 93 0 0 0 0
nvm bcfgw 38 8 2 2 7 127 0 0 0 0
nvm bcfgw 38 8 3 2 7 127 0 0 0 0
nvm bcfgw 38 8 4 4 0 0 0 0 0 0
nvm bcfgw 40 30 0
nvm bcfgw 41 30 0 0
nvm bcfgw 42 30 0 0
nvm bcfgw 45 30 85 3 0
nvm bcfgw 46 30 0
//
nvm bcfgw 0 31 17 19
nvm bcfgw 1 31 43 43
nvm bcfgw 2 31 1 1 1
nvm bcfgw 3 31 0
nvm bcfgw 4 31 0 0 0 0 0
nvm bcfgw 5 31 0
nvm bcfgw 6 31 23 7 -3 -54
nvm bcfgw 7 31 1 2
nvm bcfgw 8 31 0 262144 524288
nvm bcfgw 33 31 0 0 0 0 0 0 0 0 0 0 0 0 0
nvm bcfgw 34 31 0 0 0 0 0 0 0 0
nvm bcfgw 39 31 1 10009 15 47 16 134
nvm bcfgw 38 9 0 0 0 1 0 0 0 0
nvm bcfgw 38 9 1 2 5 93 0 0 0 0
nvm bcfgw 38 9 2 2 5 127 0 0 0 0
nvm bcfgw 38 9 3 2 7 127 0 0 0 0
nvm bcfgw 38 9 4 4 0 0 0 0 0 0
nvm bcfgw 40 31 0
nvm bcfgw 41 31 0 0
nvm bcfgw 42 31 0 0
nvm bcfgw 45 31 85 3 0
nvm bcfgw 46 31 0
nvm bcfgw 3 32 12582916 16
nvm bcfgw 43 27
nvm bcfgw 47 1

dm_en
nvm bcfgsv 1
nvm calsv 1
