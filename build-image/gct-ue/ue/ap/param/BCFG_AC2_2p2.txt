nvm bcfgw 48 1

//
// NV version
//
nvm calw 0 18

//
// Board configuration NV parameter
//
//
nvm bcfgw 0 6 3 4
nvm bcfgw 1 6 4 4
nvm bcfgw 2 6 0 0 1
nvm bcfgw 3 6 0
nvm bcfgw 4 6 0 0 0 0 0
nvm bcfgw 5 6 0
nvm bcfgw 6 6 23 15 5 -54
nvm bcfgw 7 6 2 4
nvm bcfgw 8 6 8 0 0
nvm bcfgw 33 6 1 0 0 0 0 0 0 2 0 0 0 0 0
nvm bcfgw 34 6 0 1 2 0 0 0 0 0
nvm bcfgw 35 2 2 26 34 52 11 0
nvm bcfgw 36 2 0 1 0 0
nvm bcfgw 37 2 11 11 0 0
nvm bcfgw 39 6 0 0 0 0 0 0
nvm bcfgw 40 6 128
nvm bcfgw 41 6 0 6
nvm bcfgw 42 6 0 0
nvm bcfgw 45 6 85 3 0
nvm bcfgw 46 6 0
//
nvm bcfgw 0 7 12 13
nvm bcfgw 1 7 4 4
nvm bcfgw 2 7 0 0 1
nvm bcfgw 3 7 0
nvm bcfgw 4 7 0 0 0 0 0
nvm bcfgw 5 7 0
nvm bcfgw 6 7 23 15 5 -54
nvm bcfgw 7 7 1 2
nvm bcfgw 8 7 64 0 0
nvm bcfgw 33 7 1 0 0 0 0 0 0 3 0 0 0 0 0
nvm bcfgw 34 7 1 1 3 0 0 0 0 0
nvm bcfgw 35 3 2 26 34 52 11 0
nvm bcfgw 36 3 0 1 0 0
nvm bcfgw 37 3 11 11 0 0
nvm bcfgw 39 7 0 0 0 0 0 0
nvm bcfgw 40 7 0
nvm bcfgw 41 7 0 2
nvm bcfgw 42 7 0 0
nvm bcfgw 45 7 85 3 0
nvm bcfgw 46 7 0
//
nvm bcfgw 43 27
nvm bcfgw 47 1

dm_en
nvm bcfgsv 1
