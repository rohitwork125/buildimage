#!/bin/bash

ps aux | grep -i qemu-system | awk '{print $2}' | xargs kill -9 2> /dev/null

killall -9 lted 2> /dev/null
killall -9 lteatcm lteautocm lted_cli 2> /dev/null

if [ -f gdmlte.ko ]; then
rmmod gdmlte 2> /dev/null
else
ret=`lsmod | grep gdmlte`
if [ "$ret" != "" ]; then
	rmmod gdmlte 2> /dev/null
fi
fi

#ipcs -m -s
#ipcrm -a

rm -rf /var/tmp/*

