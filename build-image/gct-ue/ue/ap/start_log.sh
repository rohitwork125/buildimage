#!/bin/bash

FILE_SYSLOG_MASK_TEMP=./logconfig_temp
FILE_SYSLOG_MASK_TEMP2=./logconfig_temp2
FILE_SYSLOG_MASK=${FILE_SYSLOG_MASK_PATH}

LOG="0"

set_syslog_mask()
{
        rm $FILE_SYSLOG_MASK 2> /dev/null
        SYSLOG_MASK=`ucfg get config syslog mask | awk '{print $1}'`
	echo "SYSLOG_MASK : ${SYSLOG_MASK}"
        SYSLOG_MASK=${SYSLOG_MASK#mask=}
	echo "syslog mask : ${SYSLOG_MASK}"
        if [ "$SYSLOG_MASK" = "" ]; then
                echo "Use normal log configuration"
        else
                echo "Use manual log configuration"
                echo $SYSLOG_MASK > $FILE_SYSLOG_MASK_TEMP
                # Mask sample is '*,x/lted,7/...
                # Token delimeter of ucfg log mask is '/' and it is 1-line string
                # So, '/' should be change CR and ',' should be change TAB.
                cat $FILE_SYSLOG_MASK_TEMP | awk '{print $0}' | sed -e 's/\//\n/g' > $FILE_SYSLOG_MASK_TEMP2
                cat $FILE_SYSLOG_MASK_TEMP2 | awk '{print $0}' | sed -e 's/,/\t/g' > $FILE_SYSLOG_MASK

                rm $FILE_SYSLOG_MASK_TEMP
                rm $FILE_SYSLOG_MASK_TEMP2
                cat $FILE_SYSLOG_MASK
        fi
}

FILE_LTE_INIT_SCRIPT_TEMP=./ltelogconfig_temp

rm -rf ./lte_init_script
touch ./lte_init_script

make_lte_init_script()
{
	echo "Make lte init script"
	GET_UCFG_STR=`ucfg get config syslog lte_init_script | awk 'BEGIN {FS="\n"} {print substr($1,17)}'`
	echo "GET_UCFG_STR : ${GET_UCFG_STR}"

	echo "${GET_UCFG_STR}" > $FILE_LTE_INIT_SCRIPT_TEMP

    cat $FILE_LTE_INIT_SCRIPT_TEMP | awk '{print $0}' | sed -e 's/\//\n/g' >"./lte_init_script"

    rm $FILE_LTE_INIT_SCRIPT_TEMP
}

main()
{
        set_syslog_mask
		make_lte_init_script
}

main

