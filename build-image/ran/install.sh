#yum install -y centos-release-scl
#yum-config-manager --enable rhel-server-rhscl-7-rpms
#yum install -y rh-python35
#scl enable rh-python35 bash

yum install -y epel-release
yum update -y
#yum install -y python3-pip
yum install -y ninja-build
yum install -y meson
yum install -y log4cxx
yum install -y log4c.x86_64
yum install -y numactl-devel
yum install -y wget vim
yum install -y jansson-devel
yum install -y iproute
yum install -y pciutils
yum install -y which
yum -y install gcc
yum -y install make
yum group install "Development Tools"

wget https://fast.dpdk.org/rel/dpdk-18.11.11.tar.xz
tar -xvf dpdk-18.11.11.tar.xz
rm -f dpdk-18.11.11.tar.xz
cd dpdk-stable-18.11.11

mkdir build
meson build

cd build
ninja
ninja install
ldconfig

#yum -y install gcc

yum install -y libconfig

yum clean all

rm -rf /var/cache/yum
