#!/bin/bash
#If not already running then start it!
#pid=$(pidof ranv_module)
#if [[ "$pid" == "" ]]; then
  #cd "${0%/*}"
  #../bin/ranv_module
  #echo "Running on pid: '$(pidof ranv_module)'"
#else
   #echo "Already running on pid: '$pid'"
#fi


BASEDIR="/opt/netprizm2/network/vphy/ranv"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/ranv_$(date "+%F-%T").log"

CURDIR=$PWD
CURPID=$(pidof ranv_module)
if [ $CURPID ]; then
   echo "INFO: RANV is already running, PID=$CURPID"
   exit
else
   cd "$BINDIR" #This is needed for Netprizm Controller to work
   unbuffer ./ranv_module 2>&1 > $LOGFILE &
   sleep 2
   echo "Running on pid: '$(pidof ranv_module)'"
fi
