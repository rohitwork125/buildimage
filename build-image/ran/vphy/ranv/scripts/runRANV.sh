#!/bin/bash
#If not already running then start it!
pid=$(pidof ranv_module)
if [[ "$pid" == "" ]]; then
  cd "${0%/*}"
  ../build/ranv_module
else
   echo "Already running on pid: '$pid'"
fi

