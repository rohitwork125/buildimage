#!/bin/bash
BASEDIR="${0%/*}/.."   #"/opt/netprizm2/network/vphy/csm"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/csm_ranv_$(date "+%F-%T").log"

CURPID=$(pidof csm_module)
if [ $CURPID ]; then
   echo "INFO: CSM is already running, PID=$CURPID"
   exit
else
   cd $BINDIR
   #unbuffer taskset -c 26 ./csm_module 2>&1 > $LOGFILE &
   unbuffer ./csm_module 2>&1 > $LOGFILE &
   sleep 1
   echo "Running on pid: '$(pidof csm_module)'"
fi

