#!/bin/bash
BASEDIR="${0%/*}/.."   #"/opt/netprizm2/network/vphy/pubsub_broker/"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/pubsub_broker_$(date "+%F-%T").log"

CURPID=$(pidof extbroker)
if [ $CURPID ]; then
   echo "INFO: Broker PubSub is already running, PID=$CURPID"
   exit
else
   cd $BINDIR
   unbuffer ./extbroker 2>&1 > $LOGFILE &
   sleep 1 #Wait then echo it is running. Needed for Netprizm Controller to work.
   echo "Running on pid: '$(pidof extbroker)'"
fi

