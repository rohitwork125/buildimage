#!/bin/bash
export LD_LIBRARY_PATH=.:/usr/local/lib:/usr/local/lib64:/usr/lib:/usr/lib64
export PKG_CONFIG_PATH=/opt/dpdk-18.11/build/meson-private

BASEDIR="${0%/*}/.."   #"/opt/netprizm2/network/vphy/pubsub_ranv"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/pubsub_ranv_$(date "+%F-%T").log"

CURPID=$(pidof localPubSub)
if [ $CURPID ]; then
   echo "INFO: RANV PubSub is already running, PID=$CURPID"
   exit
else
   #cd "${0%/*}"  #This is needed for Netprizm Controller to work
   cd $BINDIR
   #unbuffer taskset --cpu-list 23-27 $BINDIR/localPubSub -c 0x3 –socket-mem=8192,8192 2>&1 > $LOGFILE &
   unbuffer taskset --cpu-list 33-37 ./localPubSub -c 0x3 –socket-mem=8192,8192 2>&1 > $LOGFILE &
   sleep 5  #Wait then echo it is running. Needed for Netprizm Controller to work
   echo "Running on pid: '$(pidof localPubSub)'"
fi
