apt update
apt install -y apt-utils
apt install -y build-essential
apt install -y manpages-dev
apt install -y pkg-config #this is not required in dpdk
apt install -y libconfig-dev
apt install -y libnuma-dev
apt install -y liblog4c-dev
apt install -y wget vim

apt install -y python3-pip
pip3 install meson
pip3 install ninja

cd /
wget https://fast.dpdk.org/rel/dpdk-18.11.11.tar.xz
tar -xvf dpdk-18.11.11.tar.xz
rm -f dpdk-18.11.11.tar.xz
cd dpdk-stable-18.11.11

sed -i -e "s/CONFIG_RTE_LIBRTE_IP_FRAG_MAX_FRAG=4/CONFIG_RTE_LIBRTE_IP_FRAG_MAX_FRAG=64/g" config/common_base
sed -i -e "s|#define RTE_LIBRTE_IP_FRAG_MAX_FRAG 4|#define RTE_LIBRTE_IP_FRAG_MAX_FRAG 64|g" config/rte_config.h
sed -i -e "s/CONFIG_RTE_LIBRTE_TELEMETRY=n/CONFIG_RTE_LIBRTE_TELEMETRY=y/g" config/common_base

mkdir build
meson build -Dbuildtype=debug

cd build
ninja
ninja install
ldconfig

apt update
apt install -y python
apt install -y pciutils
apt install -y iproute2
apt install -y iputils-ping

apt clean

rm -rf /var/lib/apt/lists/*
