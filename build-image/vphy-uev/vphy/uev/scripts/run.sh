#!/bin/sh
BASEDIR="${0%/*}/.."   #"/opt/netprizm2/network/vphy/uev"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/uev_$(date "+%F-%T").log"

CURPID=$(pidof uev_module)
if [ $CURPID ]; then
   echo "INFO: UEV is already running, PID=$CURPID"
   exit
else
   cd $BINDIR
   unbuffer ./uev_module 1 0 2>&1 > $LOGFILE &
   sleep 2
   echo "Running on pid: '$(pidof uev_module)'"
fi

