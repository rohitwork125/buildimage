#!/bin/sh
BASEDIR="${0%/*}/.."   #"/opt/netprizm2/network/vphy/pubsub_uev"
BINDIR="${BASEDIR}/bin"
LOGDIR="${BASEDIR}/log"
LOGFILE="${LOGDIR}/pubsub_uev_$(date "+%F-%T").log"

CURPID=$(pidof localPubSub)
if [ $CURPID ]; then
   echo "INFO: UEV PubSub is already running, PID=$CURPID"
   exit
else
   #cd "${0%/*}"  #This is needed for Netprizm Controller to work
   cd $BINDIR
   #unbuffer taskset --cpu-list 23-27 $BINDIR/localPubSub -c 0x3 –socket-mem=8192,8192 2>&1 > $LOGFILE &
   unbuffer taskset --cpu-list 33-37 ./localPubSub -c 0x3 –socket-mem=8192,8192 2>&1 > $LOGFILE &
   sleep 15  #Wait then echo it is running. Needed for Netprizm Controller to work
   echo "Running on pid: '$(pidof localPubSub)'"
fi

